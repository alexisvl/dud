# intellectual property is bullshit bgdc

import os
import subprocess
import sys
import time

import git

from . import get_repo, issue_files, views
from .errors import *
from .verbose import verb

def usage(full=False):
    print("Usage: dud -h|COMMAND ...")

    if full:
        print()
        print("Commands:")
        for i in COMMANDS.keys():
            print(" ", i)

def dud(argv=None):
    if argv is None:
        argv = sys.argv

    if len(argv) == 1:
        usage()
        return
    elif len(argv) == 2 and argv[1] in ("-h", "--help"):
        usage(full=True)
        return

    try:
        func = COMMANDS[argv[1]]
    except KeyError:
        print("dud: invalid command:", argv[1])
        return 1

    try:
        repo = get_repo.get_repo(".")
        return func(repo, argv[2:])
    except DudError as e:
        print("dud:", str(e))
        return 1

def is_help(args):
    """Return whether this arg list is a single request for help"""
    return len(args) == 1 and args[0] in ("-h", "--help")

def launch_editor(fn):
    """Launch the system editor to edit a file"""

    editor = os.getenv("EDITOR")

    if not editor:
        # TODO we can probably use notepad on windows? I don't have a windows
        # box to test on.
        candidates = ["nano", "vim"]
        for i in candidates:
            if shutil.which(i):
                editor = i
                break
        else:
            raise DudError("Can't find editor for:", fn)

    subprocess.run([editor, fn])

def cmd_init(repo, args):
    # Just loading the repo does the initialization, this is a no-op.
    pass

def cmd_new(repo, args):
    if is_help(args):
        print("Usage: dud new [TITLE]")
        return

    num = issue_files.new_issue(repo)
    if args:
        title = args[0]
    else:
        title = input("Title: ").strip()

    fn = issue_files.make_fn(repo, num, title)

    author = get_repo.get_author(repo)
    timestamp = time.strftime("%a %b %d %H:%M:%S %Z %Y")

    with open(fn, "w") as f:
        f.write(f"Title:    {title}\n")
        f.write(f"Tags:     open\n")
        f.write(f"Author:   {author.name} <{author.email}>\n")
        f.write(f"Assignee: {author.name}\n")
        f.write(f"Created:  {timestamp}\n")
        f.write("\n")
        f.write("Description goes here. To cancel, delete the entire file contents.\n")

    launch_editor(fn)

    with open(fn) as f:
        if not f.read().strip():
            os.unlink(fn)
            raise DudError("Issue canceled.")

    update_index(repo)
    commit_file(repo, fn, f"dud: add {num}: {title}", True, remove_first=False)

def cmd_view(repo, args):
    USAGE = "Usage: dud view DEST table OPTIONS..."

    if is_help(args) or len(args) < 2:
        print(USAGE)
        return

    dest = args[0]
    fmt  = args[1]
    fmt_args = args[2:]

    view_functions = {
        "table": views.view_table,
    }

    if fmt not in view_functions:
        print(USAGE)
        return 1

    output = view_functions[fmt](repo, dest, fmt_args)

def cmd_table(repo, args):
    if is_help(args):
        print("Usage: dud table OPTIONS...")
        return

    print(views.view_table(repo, None, args))

def cmd_edit(repo, args):
    if is_help(args) or not args:
        print("Usage: dud edit NUM")
        return

    try:
        num = int(args[0])
    except ValueError:
        raise DudError(f"Invalid issue number: {args[0]!r}")

    path = issue_files.path_to(repo, num)
    if path is None:
        raise DudError(f"No such issue: {num}")

    with open(path) as f:
        content = f.read()

    launch_editor(path)

    with open(path) as f:
        if f.read() != content:
            verb("adding commit for modified issue")
            entry = issue_files.load_issue(path)

            update_index(repo)
            commit_file(repo, path, f"dud: edit {num}: {entry.title}", True)
        else:
            verb("issue not modified")

def cmd_tag(repo, args):
    if is_help(args) or not args:
        print("Usage: dud tag NUM [+tag | -tag ...]")
        return

    try:
        num = int(args[0])
    except ValueError:
        raise DudError(f"Invalid issue number: {args[0]!r}")

    path = issue_files.path_to(repo, num)
    if path is None:
        raise DudError(f"No such issue: {num}")

    for i in args[1:]:
        if not i or i[0] not in "+-":
            raise DudError(f"Invalid tag option, must be +tag or -tag: {i}")

    entry = issue_files.load_issue(path)

    tags = entry.tags

    if len(args) == 1:
        # List the tags
        print(*tags)
        return
    else:
        for i in args[1:]:
            tagname = i[1:]
            if i[0] == "+" and tagname not in tags:
                tags.append(tagname)
            elif i[0] == "-":
                tags.remove(tagname)

        entry.tags = tags
        entry.write()

        tagmsg = " ".join(args[1:])
        update_index(repo)
        commit_file(repo, path, f"dud: tag {num} {tagmsg}: {entry.title}", True)

def cmd_close(repo, args):
    if is_help(args) or len(args) != 1:
        print("Usage: dud close NUM")
        return

    return cmd_tag(repo, [args[0], "-open", "+closed"])

def commit_file(repo, path, message, relocate, remove_first=True):
    """Add a commit for an issue file.

    path:     path to the file
    message:  full commit message
    relocate: whether to relocate the file according to the tag directories
    remove_first: whether the file needs to be removed from the index before
                  moving
    """

    if relocate:
        entry = issue_files.load_issue(path)
        tags = entry.tags
        if repo.dud_config["dud"]["move_to_root"]:
            location = repo.working_tree_dir
        else:
            location = os.path.dirname(path)
        for tagdir in repo.dud_config["dud"]["tag_directories"]:
            if tagdir in tags:
                location = os.path.join(repo.working_tree_dir, tagdir)
                break
        if not os.path.isdir(location):
            os.mkdir(location)
        new_path = os.path.join(location, os.path.basename(path))
        verb(f"relocate {path} -> {new_path}")
        if os.path.exists(path) and remove_first:
            repo.index.remove(path)
        os.rename(path, new_path)
        repo.index.add(new_path)
    else:
        repo.index.add(path)

    author = get_repo.get_author(repo)
    repo.index.commit(
        message,
        author = author,
        committer = author,
    )
    get_repo.push_repo(repo)

def update_index(repo):
    """Update the index in the repository."""

    try:
        repo.index.remove("index.md")
    except git.exc.GitCommandError:
        pass
    views.generate_index(repo)
    repo.index.add("index.md")

COMMANDS = {
    "init":  cmd_init,
    "new":   cmd_new,
    "view":  cmd_view,
    "table": cmd_table,
    "edit":  cmd_edit,
    "tag":   cmd_tag,
    "close": cmd_close,
}
