# intellectual property is bullshit bgdc

import contextlib
import copy
import os
import shutil
import toml
import urllib

import git
from git.util import Actor

from .errors import *
from .verbose import verb, warn

DUD_BRANCH_NAME = "dud"
DUD_CACHE_DIR   = "$HOME/.dud/cache"

CONFIG_FILE_TEMPLATE = """[dud]
# Any tags mentioned here will result in issues with that tag being placed in
# a tag directory. If an issue matches multiple, it will go in the first one
# listed.
tag_directories = ["closed"]

# If true, issues not matching any tag_directories will be moved to the root.
# If false, they'll be left where they are.
move_to_root = true
"""

DEFAULT_CONFIG = { "dud": {
    "tag_directories": ["closed"],
    "move_to_root":    True,
}}

def get_repo(path, interactive=True):
    """Return a Repo object pointing at the repository and with the dud
    branch checked out.

    If path points to a repo with a dud branch but it's not checked out,
    the repository will be cloned to a cache location with the branch checked
    out there.

    Two attributes are added to the Repo:
        .dud_parent - contains None if we use the repo at `path`, or contains
            a Repo from which the cache was created.
        .dud_config - contains the loaded configuration data.

    If no dud branch exists, and interactive is true, the user will be prompted
    to create it. Otherwise, this is a failure and an exception will be raised.
    """

    repo = git.Repo(path)

    if not DUD_BRANCH_NAME in repo.heads:
        if interactive:
            print(f"No '{DUD_BRANCH_NAME}' branch exists on the current repository.")
            resp = input("Create it (y/n)? ")
            if resp.strip().lower() == "y":
                return create_repo_branch(repo)
        raise DudError(f"No '{DUD_BRANCH_NAME}' branch exists.")

    else:
        try:
            branch = repo.active_branch
        except TypeError:
            # HEAD is detached ... why is this a TypeError
            verb("HEAD is detached, will create a cache")
            branch = None

        if branch == DUD_BRANCH_NAME:
            verb("branch already checked out, using it directly")
            repo.dud_parent = None
            load_config(repo)
            return repo
        else:
            return load_cache(repo)

def load_config(repo):
    """Load the configuration for this repo."""

    repo.dud_config = copy.deepcopy(DEFAULT_CONFIG)

    path = os.path.join(repo.working_tree_dir, "dud.toml")
    try:
        with open(path) as f:
            dud_config = toml.load(f)
            if "dud" not in repo.dud_config:
                repo.dud_config = {}
            repo.dud_config["dud"].update(dud_config["dud"])
    except FileNotFoundError:
        warn("dud.toml not found, using default configuration")
        repo.dud_config = DEFAULT_CONFIG

def load_cache(repo):
    """Load a cache repo for this repo. If it doesn't exist, it will be
    created.
    """

    loc = cache_for(repo)

    try:
        cache = git.Repo(loc)
        cache.dud_parent = repo
        verb("loaded existing cache")
        pull_repo(cache)
    except (git.exc.InvalidGitRepositoryError, IndexError):
        if os.path.exists(loc):
            # We had a cache, but when we tried to update the dud branch, it
            # failed. This probably means the branch was deleted locally but not
            # in the cache.
            verb("clearing stale cache")
            shutil.rmtree(loc)
            os.mkdir(loc)

        cache = git.Repo.init(loc)
        cache.create_remote(
            name="dud_local",
            url=repo.working_tree_dir,
        )
        cache.dud_parent = repo
        verb("created new cache")
        pull_repo(cache, allow_checkout_fail=True)

    load_config(cache)
    return cache

def create_repo_branch(repo):
    """Add a dud branch to repo. The repo will be cloned into a cache in the
    process, and this new clone is returned.
    """

    cache = load_cache(repo)

    if os.path.isfile("dud"):
        verb("marker file already exists in cache, was the branch deleted?")
    else:
        verb("creating config file on new branch")
        with contextlib.chdir(cache.working_tree_dir):
            with open("dud.toml", "w") as f:
                f.write(CONFIG_FILE_TEMPLATE)

        actor = get_author(cache)
        cache.index.add("dud")
        cache.index.commit(
            "dud: initialize branch",
            author=actor,
            committer=actor,
        )

    h = cache.create_head(DUD_BRANCH_NAME)
    h.checkout()
    push_repo(cache)

def cache_for(repo):
    """Return the path to the cache location for this repo."""

    cache_root = os.path.expandvars(DUD_CACHE_DIR)
    name = repo.working_tree_dir
    mangled = urllib.parse.quote(name, safe="")
    path = os.path.join(cache_root, mangled)
    if not os.path.isdir(path):
        os.makedirs(path, exist_ok=True)
    verb("using cache path:", path)
    return path

def push_repo(repo):
    """If the repo has a dud parent, push to it."""

    if repo.dud_parent is not None:
        verb("pushing cache to local")
        repo.git.push("dud_local", DUD_BRANCH_NAME)

def pull_repo(repo, allow_checkout_fail=False):
    """If the repo has a dud parent, pull from it.

    allow_checkout_fail: if true, it's ok if the parent doesn't have the branch
    """

    if repo.dud_parent is not None:
        verb("pulling cache from local")
        repo.remotes.dud_local.fetch(prune=True)
        if DUD_BRANCH_NAME in repo.remotes.dud_local.refs or not allow_checkout_fail:
            try:
                h = repo.create_head(
                    DUD_BRANCH_NAME,
                    repo.remotes.dud_local.refs[DUD_BRANCH_NAME]
                )
                h.checkout()
                verb(f"created new ref {DUD_BRANCH_NAME}")
            except OSError:
                # ref already exists
                repo.refs[DUD_BRANCH_NAME].checkout()
                repo.head.reset(
                    repo.remotes.dud_local.refs[DUD_BRANCH_NAME],
                    index=True,
                    working_tree=True,
                )
                verb(f"reset ref {DUD_BRANCH_NAME}")

def get_author(repo):
    """Get the correct author for commits on this repo.

    TODO: should be possible to pass an argument to override this.
    """

    # The user could have set a different commiter than their global default,
    # so ask the original repo who it thinks the author should be.
    if repo.dud_parent is not None:
        repo = repo.dud_parent

    name  = repo.git.config("user.name")
    email = repo.git.config("user.email")
    verb(f"author is {name} <{email}>")
    return Actor(name, email)
