# intellectual property is bullshit bgdc

import contextlib
import os
import re

import markdown
import slugify

from .errors import *

ISSUE_FN_RE = re.compile(r"(\d+)-(.*)\.md$")

class IssueEntry:
    def __init__(self, path, num, slug, md, html):
        self.path = path
        self.num  = num
        self.slug = slug
        self.md   = md
        self.html = html

    @property
    def title(self):
        return self.md.Meta.get("title", [self.slug])[0]

    @property
    def author(self):
        return self.md.Meta.get("author", [""])[0]

    @property
    def assignee(self):
        return self.md.Meta.get("assignee", [""])[0]

    @property
    def tags(self):
        """Return the tags from a markdown object"""
        T = []
        for i in self.md.Meta.get("tags", []):
            T.extend(i.split())
        return T

    @tags.setter
    def tags(self, v):
        """Set the tags on this entry"""
        self.md.Meta["tags"] = [" ".join(v)]

    def write(self):
        """Write the file back out.

        Note that the markdown library doesn't preserve quite enough information
        to do this perfectly, so slight reformatting may occur (mainly of the
        metadata section, and any blank lines at the end will be stripped).
        """

        with open(self.path, "w") as f:
            max_key = max(len(k) for k in self.md.Meta.keys())
            for k, v in self.md.Meta.items():
                # The meta extension lowercases all keys, but I'm capitalizing
                # them.
                k = k[0].upper() + k[1:]
                if not v:
                    v = [""]
                f.write(f"{k + ':':{max_key + 2}}{v[0]}\n")
                indent = max(max_key + 2, 4) * " "
                for i in v[1:]:
                    f.write(f"{indent}{i}\n")
            f.write("\n")
            while self.md.lines and not self.md.lines[-1]:
                del self.md.lines[-1]
            for line in self.md.lines:
                f.write(line)
                f.write("\n")

def issue_paths(repo):
    """Yield each filename for issues in the repo"""

    for root, dirs, files in os.walk(repo.working_tree_dir):
        for fn in files:
            if not ISSUE_FN_RE.match(fn):
                continue
            yield os.path.join(os.path.join(root, fn))

def all_issues(repo):
    """Yield each filename for issues in the repo as an IssueEntry"""

    for path in issue_paths(repo):
        yield load_issue(path)

def load_issue(path):
    """Load one issue from path to an IssueEntry"""

    name = os.path.basename(path)
    m = ISSUE_FN_RE.match(name)

    if m is None:
        raise DudError(f"Invalid issue filename: {name}")

    num  = int(m.group(1))
    slug = m.group(2)
    with open(path) as f:
        md = markdown.Markdown(
            extensions=["meta"],
        )
        html = md.convert(f.read())
    return IssueEntry(path, num, slug, md, html)

def new_issue(repo):
    """Allocate a new issue number on this repository.

    Scans all issue files for their numbers, and returns the first integer
    not found.
    """

    known_issues = []

    for path in issue_paths(repo):
        fn = os.path.basename(path)
        m = ISSUE_FN_RE.match(fn)
        if m is None:
            continue
        known_issues.append(int(m.group(1)))

    known_issues.sort()
    num = 1
    for i in known_issues:
        if i == num:
            num = i + 1

    return num

def path_to(repo, num):
    """Return the path to an issue by number, or None if not found."""

    for path in issue_paths(repo):
        fn = os.path.basename(path)
        m = ISSUE_FN_RE.match(fn)
        if m is None:
            continue
        if int(m.group(1)) == num:
            return path

def make_fn(repo, number, title):
    """Return the path to an issue given number and title."""

    return os.path.join(repo.working_tree_dir, f"{number}-{san(title)}.md")

def san(title):
    """Sanitize a title for use in a filename."""

    return slugify.slugify(title)
