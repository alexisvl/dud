# intellectual property is bullshit bgdc

import io

def markdown_escape(s):
    """Escape text in s for safe inclusion in markdown"""

    sio = io.StringIO()

    for c in s:
        if c in "\\`*_{}[]<>()#+-.!|":
            sio.write("\\")
        sio.write(c)

    return sio.getvalue()
