# intellectual property is bullshit bgdc

import collections
import contextlib
import os
import re

import markdown
import prettytable

from . import issue_files, get_repo, util
from .errors import *
from .verbose import *

class AuthAssignFilter:
    ACTOR_RE = re.compile(r"(?P<name>[^<>]+)?\s?(<(?P<email>[^<>]+)>)?")
    WARNED_ABOUT = []

    def __init__(self, repo, is_negated, is_assignee, name):
        self.is_negated = is_negated
        self.is_assignee = is_assignee

        if name == "me":
            author = get_repo.get_author(repo)
            self.name = author.name
            self.email = author.email
        else:
            m = AuthAssignFilter.ACTOR_RE.match(name)
            if m is None:
                raise DudError(f"Can't parse actor (expected name <email>): {name}")
            self.name = m.group("name").strip()
            self.email = m.group("email").strip()

    def match(self, issue):
        key = issue.assignee if self.is_assignee else issue.author
        m = AuthAssignFilter.ACTOR_RE.match(key)

        if m is None and issue not in AuthAssignFilter.WARNED_ABOUT:
            verbose.warning(f"Can't parse author or assignee on issue {issue.num}")
            AuthAssignFilter.WARNED_ABOUT.append(issue)

        matches = True
        if m.group("name") and self.name:
            if m.group("name").strip() != self.name:
                matches = False
        if m.group("email") and self.email:
            if m.group("email").strip() != self.email:
                matches = False

        return (not matches) if self.is_negated else matches

class TagFilter:
    def __init__(self, is_negated, tag):
        self.is_negated = is_negated
        self.tag = tag

    def match(self, issue):
        has_tag = self.tag in issue.tags
        return (not has_tag) if self.is_negated else has_tag

def filter(repo, items, args):
    """Apply the filters in args to the items, yielding those that pass filters.

    Filters include:
        author[!]=     Accepts name, <email>, name <email>, or "me"
        assignee[!]=   Accepts name, <email>, name <email>, or "me"
        tag[!]

    By default, `closed!` is included, so only open issues are shown. To negate
    this, `closed` can be used. The special argument `any` removes this
    filter without replacing it.
    """

    filters = []
    open_only = True

    for i in args:
        if i.startswith("author="):
            filters.append(AuthAssignFilter(repo, False, False, i[7:]))
        elif i.startswith("author!="):
            filters.append(AuthAssignFilter(repo, True, False, i[8:]))
        elif i.startswith("assignee="):
            filters.append(AuthAssignFilter(repo, False, True, i[9:]))
        elif i.startswith("assignee!="):
            filters.append(AuthAssignFilter(repo, True, True, i[10:]))
        elif i == "any":
            open_only = False
        elif ("=" not in i) and i.endswith("!"):
            filters.append(TagFilter(True, i[:-1]))
        elif "=" not in i:
            if i == "closed":
                open_only = False
            filters.append(TagFilter(False, i))
        else:
            warn("unknown filter:", i)

    if open_only:
        filters.append(TagFilter(True, "closed"))

    for item in items:
        for f in filters:
            if not f.match(item):
                break
        else:
            yield item

def sort(items, args):
    """Sort the items according to the arguments, returning a list"""

    items = list(items)

    # TODO there are no sort args yet
    items.sort(key=lambda e: e.num)
    return items

def create_issue_table(repo, args, link):
    """Return a PrettyTable of issues.

    link: if true, issue titles will be links
    """

    items = sort(filter(repo, issue_files.all_issues(repo), args), args)
    tab = prettytable.PrettyTable()
    tab.field_names = ["Number", "Title", "Tags", "Author"]
    tab.align = "l"

    if link:
        def link_fn(issue):
            path = issue.path
            if path.startswith(repo.working_tree_dir):
                path = path[len(repo.working_tree_dir):]
                if path.startswith("/"):
                    path = path[1:]
                return f"[{util.markdown_escape(issue.title)}]({path})"
            else:
                # Don't leak external paths accidentally
                warn("why is an issue outside the repo?", path)
                return util.markdown_escape(issue.title)
    else:
        def link_fn(issue):
            return util.markdown_escape(issue.title)

    for i in items:
        num = i.num
        title  = i.title
        author = i.author
        tags = i.tags
        tab.add_row([
            num,
            link_fn(i),
            " ".join(util.markdown_escape(tag) for tag in tags),
            util.markdown_escape(author)])

    tab.set_style(prettytable.MARKDOWN)

    return tab

def view_table(repo, dest, args):
    """Specifically for table(), dest may be None, in which case the table
    text is returned."""

    tab = create_issue_table(repo, args, False)

    if dest is None:
        return str(tab)
    else:
        with open(dest, "w") as f:
            f.write(str(tab))

def generate_index(repo):
    """Create or update index.md for this repo."""

    fn = os.path.join(repo.working_tree_dir, "index.md")
    tab_open   = create_issue_table(repo, ["closed!"], True)
    tab_closed = create_issue_table(repo, ["closed"], True)

    with open(fn, "w") as f:
        f.write("# Issues\n")
        f.write("\n")
        f.write(str(tab_open))
        f.write("\n")
        f.write("\n")
        f.write("## Closed\n")
        f.write("\n")
        f.write(str(tab_closed))
