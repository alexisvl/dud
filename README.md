# dud

`dud` is a super lightweight tool for issue/bug tracking on small projects.
Issues are managed as markdown files in an orphan branch, which you can manage
manually if you want. You can use dud commands to:

- `dud new`: create a new issue
- `dud table`: display a table of issues
- `dud view`: create an exported view of issues
- `dud close`: close an issue (this is an abbreviation of `dud tag`)
- `dud tag`: apply or remove tags from issues
- `dud rm`: delete an issue completely
- `dud push`: push to the remote (this is just a `git push` of the `dud` branch)
- `dud pull`: pull from the remote (this is just a `git pull` of the `dud` branch)

If the current repository does not have a `dud` branch, dud will ask if you
want to create it. If it does have one, and that branch is currently checked
out, dud will work in the current repository, otherwise dud will clone the
repository in a local cache and check out its branch.

dud's issues are tracked with dud itself:

[Issues](https://gitlab.com/alexisvl/dud/-/tree/dud?ref_type=heads)

## Views

Views are various exports dud can create from the issues. When creating a
view, you can apply filters. Supported views include:

- `dud view html`: render issues to static HTML
- `dud view ln`: create a directory containing symlinks to matching issues
- `dud view table`: create a plaintext table of issues (`dud table` is just this
  plus a pager)

## Configuration

dud is configured by a set of toml configuration files. These are loaded in
the following order, with each one overriding the ones before it:

- `$HOME/.dudrc`
- `$REPO/dudrc`
- `$REPO/.dudrc`

All config keys are defined under one table `[dud]`. They are:

(none currently)
