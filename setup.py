from setuptools import setup

import sys

setup(
    name='dud',
    description='simple git issue tracker',
    url='https://gitlab.com/alexisvl/dud',
    author='xan',
    author_email='xan@fip.zone',
    packages=['dud'],
    entry_points = {
        'console_scripts': [
            'dud = dud:run',
            ],
    },
)
